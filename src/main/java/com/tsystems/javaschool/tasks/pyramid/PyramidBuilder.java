package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        
		if(inputNumbers == null) {
			throw new CannotBuildPyramidException();
		}
		
        int[][] output = getArray(inputNumbers);
		
		try {
			Collections.sort(inputNumbers); // I supposed that is longest part of algorithm
		} catch (NullPointerException e){
			throw new CannotBuildPyramidException();
		}		

		return fillPyramidArray(output, inputNumbers);
	}
	
	private int[][] getArray(List<Integer> inputNumbers){
		int n = 1;
		int basement = 1;
		int leftSize = inputNumbers.size();
		
		while(true) {
			if(leftSize < n) {
				throw new CannotBuildPyramidException(); 
			}
			if(leftSize == n) {
				break;
			}
			leftSize -= n++;
			basement += 2;
		}		

		return new int[n][basement];
	}
	
	private int[][] fillPyramidArray(int[][] output, List<Integer> inputNumbers){
		int yPos = 0;		
		int inputPos = 0;		
	
		for(int counter = 0; counter < output.length; counter++) {
			int xPos = output[0].length/2 - yPos;	 // middle at 0 row, and then we will shift to right by 1 for each next row					
			for(int i = 0; i < yPos + 1; i++) { //qty of numbers placed to current row
				output[yPos][xPos] = inputNumbers.get(inputPos++);
				xPos += 2;
			}
			yPos++;	
		}
		
		return output;
	}
}
