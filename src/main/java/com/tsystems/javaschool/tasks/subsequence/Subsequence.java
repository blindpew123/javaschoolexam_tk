package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(x == null || y == null) {
			throw new IllegalArgumentException("At least, one argument is null reference");
		}
		
		boolean result = false;
		if(x.size()<=y.size()) {
			if (x.isEmpty()) {
				return true;
			}			
			int posX = 0;
        	for(int posY = 0; posY < y.size(); posY++) {
        		if (y.get(posY).equals(x.get(posX)) && (++posX == x.size())) {
        				return true;
        			}
        		}
        	}
		return result;
    }
}
