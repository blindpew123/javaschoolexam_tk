package com.tsystems.javaschool.tasks.calculator.calculation;

/**
 * Names of math operations and information about evaluation order
 * also, in some cases, uses to get information about possibility to replace MINUS to UNARY_MINUS
 */

enum OperationName {
	MINUS {
		@Override
		public boolean isCalculateBefore(OperationName op) {
			if (op == OperationName.MINUS || op == OperationName.PLUS) {
				return true;
			}
			return false;
		}
	},
	PLUS {
		@Override
		public boolean isCalculateBefore(OperationName op) {
			if (op == OperationName.MINUS || op == OperationName.PLUS) {
				return true;
			}
			return false;
		}
	},
	OPEN_PARENTHESIS {
		@Override
		public boolean isCalculateBefore(OperationName op) {			
			return false;
		}
	},
	CLOSE_PARENTHESIS {
		@Override
		public boolean isCalculateBefore(OperationName op) {
			return true;
		}
	},
	MULTIPLY {
		@Override
		public boolean isCalculateBefore(OperationName op) {
			if (op == OperationName.OPEN_PARENTHESIS) {
				return false;
			}
			return true;
		}		
	},
	DIVIDE {
		@Override
		public boolean isCalculateBefore(OperationName op) {
			if (op == OperationName.OPEN_PARENTHESIS) {
				return false;
			}
			return true;
		}
	},
	UNARY_MINUS{
		@Override
		public boolean isCalculateBefore(OperationName op) {
				return true;
		}
		
	};
	/**
	 * each OperationName token has own implementation according evaluation order
	 * @param op name of operation
	 * @return true if current operation have to calculate before op
	*/ 
	public abstract boolean isCalculateBefore(OperationName op);
	
	/**
	 *  To get information about possibility to replace MINUS to UNARY_MINUS
	 *  @param op name of operation
	 *  @return true if safe to replace MINUS to UNARY_MINUS
	 */
	public static boolean isUnaryMinusAllowedAfter(OperationName op) {
		switch(op) {
			case OPEN_PARENTHESIS:
			case MULTIPLY:
			case DIVIDE:
				return true;
			default:
				return false;
		}
	}
}