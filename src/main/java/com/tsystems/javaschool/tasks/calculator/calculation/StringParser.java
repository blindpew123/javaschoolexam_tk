package com.tsystems.javaschool.tasks.calculator.calculation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.NoSuchElementException;


/**
 * Contains single method for parsing string with arifmethic expression
 *
 */

public class StringParser {
	
	private Deque<Integer> intDigits = new ArrayDeque<>();
	private Deque<Integer> decimalDigits = new ArrayDeque<>();
	private Deque<OperationName> parentheses = new ArrayDeque<>();
	
	/**
	 * @param expr string with arifmethic expression
	 * @throws IllegalArgumentException when impossible to parse incoming string
	 * @return List<Object> tokens (Double numbers and OperationName names of operations)
	*/
	public List<Object> parse(String expr) {
		if (expr==null){
			throw new IllegalArgumentException("Expression can't be null");
		}
		List<Object> result = new ArrayList<>();
		boolean startInteger = true;		
		for(int i = 0; i<expr.length();i++) {			
			if(Character.isDigit(expr.charAt(i))) {
				if(startInteger) {
					intDigits.addLast(Character.getNumericValue(expr.charAt(i)));
				} else {
					decimalDigits.push(Character.getNumericValue(expr.charAt(i)));
				}
			} else if(expr.charAt(i)=='.') {
				if(!startInteger) {
					throw new IllegalArgumentException("Wrong decimal format!");
				}
				startInteger = false;
			} else {
				if (!intDigits.isEmpty() || !decimalDigits.isEmpty()) {
					result.add(getNumber());
				}
				result.add(getOperation(expr.charAt(i)));
				startInteger = true;
			}			
		}
		
		if (!intDigits.isEmpty() || !decimalDigits.isEmpty()) {
			result.add(getNumber());
		}
		
		if (!parentheses.isEmpty()) {
			throw new IllegalArgumentException("Wrong order of parentheses");
		}
		
		return result;
	}
	
	private Double getNumber() {
		Double intPart = 0d;
		Double decPart = 0d;
		
		while(!intDigits.isEmpty()) {
			intPart += intDigits.pop() * Math.pow(10, intDigits.size());
		}
		
		while(!decimalDigits.isEmpty()) {
			decPart += decimalDigits.pop() * Math.pow(10, (-1d) * (decimalDigits.size()+1));					
		}
		return intPart + decPart;
	}
	
	private OperationName getOperation(char op) {
		switch(op) {
			case '(':
				parentheses.push(OperationName.OPEN_PARENTHESIS);
				return OperationName.OPEN_PARENTHESIS;
			case ')':
				try {
					parentheses.pop();
					return OperationName.CLOSE_PARENTHESIS;
				} catch (NoSuchElementException e){
					throw new IllegalArgumentException("Wrong order of parentheses");
				}
			case '/':
				return OperationName.DIVIDE;
			case '*':
				return OperationName.MULTIPLY;
			case '+':
				return OperationName.PLUS;
			case '-':
				return OperationName.MINUS;
			default: throw new IllegalArgumentException("Can't parse input string");
		}
	}
}