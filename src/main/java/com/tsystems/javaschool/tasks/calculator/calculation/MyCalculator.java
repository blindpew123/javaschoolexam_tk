package com.tsystems.javaschool.tasks.calculator.calculation;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.function.Predicate;

public class MyCalculator {
	
	/**
	 * Contains main loop for processing List<object> tokens and returns Double result, if possible
	 * or null if result is not calculated. Although calculation errors in most cases will lead to the 
	 * CalculationException
	 * @param tokens List<Objects> contains Double numbers and OperationName operators;
	 * @throws CalculationException
	 * @return result of calculation
	 */
	
	public Double calc(List<Object> tokens) {	
		final Deque<Double> operandsStack = new ArrayDeque<>(); 
		final Deque<OperationName> operatorsStack = new ArrayDeque<>();
		boolean isUnaryMinusAllowed = true;	
		
		for(Object token : tokens){			
			if(token instanceof Double) {
				operandsStack.push((Double)token);
				isUnaryMinusAllowed = false; 			
			} else {								
				if((OperationName)token == OperationName.CLOSE_PARENTHESIS) {
					doCalc(operators -> operators.peek() != OperationName.OPEN_PARENTHESIS, operatorsStack, operandsStack);
					operatorsStack.pop(); //remove open parenthesis				
				} else {
					doCalc(operators -> !operators.isEmpty() && operators.peek().isCalculateBefore((OperationName)token), operatorsStack, operandsStack);
					
					if(isUnaryMinusAllowed && (OperationName)token == OperationName.MINUS) {
						operatorsStack.push(OperationName.UNARY_MINUS);
					} else {	
						operatorsStack.push((OperationName)token);
					}
				}
				isUnaryMinusAllowed = OperationName.isUnaryMinusAllowedAfter(operatorsStack.peek());				
			}
		}
		doCalc(operators -> !operators.isEmpty(), operatorsStack, operandsStack);
		
		return operandsStack.isEmpty() ? null:operandsStack.pop();
	}
	
	private void doCalc(Predicate<Deque<OperationName>> p, Deque<OperationName> operators, Deque<Double> operands) {
		while(p.test(operators)) {
			OperationFactory.getOperationByName(operators.pop()).calcPartialResult(operands);
		}
	}	
}