package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.calculation.MyCalculator;
import com.tsystems.javaschool.tasks.calculator.calculation.StringParser;
import com.tsystems.javaschool.tasks.calculator.calculation.CalculationException;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;



public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
	 
    public String evaluate(String statement) {
        StringParser parser = new StringParser();
		
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		DecimalFormat format = new DecimalFormat();
		format.setDecimalFormatSymbols(dfs);
		format.applyPattern("#.####");
		format.setDecimalSeparatorAlwaysShown(false);
		
		try {
			Double result = (new MyCalculator()).calc(parser.parse(statement));
			return format.format(result);
		} catch(IllegalArgumentException | CalculationException e) {
			return null;
		}
    }

}
