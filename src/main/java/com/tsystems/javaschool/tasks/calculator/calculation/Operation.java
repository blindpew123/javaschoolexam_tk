package com.tsystems.javaschool.tasks.calculator.calculation;

import java.util.Deque;
import java.util.NoSuchElementException;
/**
 * Interface for executing calculation operation
 * 
 */
interface Operation {	
	/**
	 * Call this method for perform calculation according implementation process(Deque<Double>) method
	 * 
	 * @param operands stack of operands for process
	 */
	public default void calcPartialResult(Deque<Double> operands) {
		try{
			process(operands);
		} catch (NoSuchElementException e) {
			throw new CalculationException();
		}
	}
	
	/**
	 * Implementation must satisfy the following conditions.
	 * result, if available, have to be placed back to stack,
	 * if operation can't be performed it should throws CalculationException,
	 * if the presence of operands in the stack is not controlled possible to throw NoSuchElementException.
	 * 
	 * @param operands stack of operands for process
	 */
	void process (Deque<Double> operands);
}