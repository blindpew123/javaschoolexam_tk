package com.tsystems.javaschool.tasks.calculator.calculation;

import java.util.Deque;

/**
 * Contains single static method for get implementation of calculation operation, represented by its name
*/
public class OperationFactory {	
	
	/**
	 * @param operationName name operation
	 * @return Operation implementation of calculation operation
	 * @see OperationName
	 */
	public static Operation getOperationByName(OperationName operationName) {
		switch(operationName) {
			case MINUS:
				return operands -> {
					double subtrahend = operands.pop();
					operands.push(operands.pop() - subtrahend);
				};
			case PLUS:
				return operands -> operands.push(operands.pop() + operands.pop());
			case MULTIPLY:
				return operands -> operands.push(operands.pop() * operands.pop());
			case DIVIDE:
				return operands ->{
					double divider = operands.pop();
					if (Double.compare(divider, 0d)==0) {
						throw new CalculationException();
					}
					operands.push(operands.pop() / divider);
				};
			case UNARY_MINUS:
				return operands -> operands.push(operands.pop() * -1);	
			default:
				throw new CalculationException();
		}	
	}
}